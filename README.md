Prueba técnica para Pavabit.

La base de datos está alojada en elephantsql.com, adjunto una captura de como está montada.

Las credenciales de la base de datos se las he pasado a la recruiter para que las pasa, en caso de no tenerlas enviarme un correo.

TODO:

Front

* User model
* Medidor de seguridad de la contraseña
* Revisar reponsive
* Revisar scafolding de los componentes
* Revisar tipos de las variables 'any'
* Warning en la Home
* Cargar los dos componentes de la home a la vez
* Control de errores al crear una medición
* Testing
  
Back

* Uso de TypeScript
* Sanitize para evitar código malicioso
* Mejorar la implementación de la base de datos
* Revisar posibles mensajes de error
* Proteger rutas con Token
* Control de errores de las mediciones
* Testing
* Documentar con Swagger
