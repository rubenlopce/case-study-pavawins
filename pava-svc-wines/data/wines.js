import * as db from '../util/database.js'

export const addMeasurament = async ( data ) => {
  db.createMeasurament(data)
  return { data }
}

export const getTypes = async () => {
  const types = await db.getTypes()
  return { types }
}

export const getVarieties = async () => {
  const varieties = await db.getVarieties()
  return { varieties }
}

export const getMeasuraments = async () => {
  const measuraments = await db.getMeasuraments()
  return { measuraments }
}

