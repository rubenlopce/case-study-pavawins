import bcrypt from 'bcryptjs'

import * as db from '../util/database.js'

export const add = async ( data ) => {  
  bcrypt.genSalt( 3, (err, salt) => {  
    bcrypt.hash(data.password, salt, (err, hash) => {
      db.createUser( data.user, hash )
    })
  })
  return { user: data.user }
}

export const get = async ( user ) => {
  const userResult = await db.getUser( user )
  if (userResult === null) {
    throw { code: 404, message: 'El usuario no existe.' }
  }
  return userResult
}

export const exist = async ( user ) => {
  const userResult = await db.getUser( user )
  if (userResult !== null) {
    throw { code: 422, message: 'El usuario ya existe.' }
  }
}
