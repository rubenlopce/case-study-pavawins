import * as winesData from '../data/wines.js'

import express from 'express'

const router = express.Router()

router
  .route('/measuraments')
    .post( async (req, res, next) => {
      const data = req.body
      try {
        const createdMeasurament = await winesData.addMeasurament(data)
        return res
                .status(201)
                .json({ message: 'Measurament added', createdMeasurament })
      } catch (error) {
        return res.json(error)
      }
    })
    .get( async (req, res, next) => {
      let measuraments
        try {
          measuraments = await winesData.getMeasuraments()
        } catch ( err ) {
          return res.status( err.code ).json( {code: err.code, message: err.message } )
        }     
        return res.json(measuraments)
    })

router.get( '/form-select', async ( req, res, next ) => {
  let varieties
  let types
  try {
    varieties = await winesData.getVarieties()
    types = await winesData.getTypes()
  } catch ( err ) {
    return res.status( err.code ).json( {code: err.code, message: err.message } )
  }     
  return res.json({...varieties, ...types})
})

export default router