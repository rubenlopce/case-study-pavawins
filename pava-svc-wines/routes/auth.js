import * as userData from '../data/user.js'
import { isValidPassword, createJSONToken } from '../util/auth.js'

import express from 'express'

const router = express.Router()

router
  .post('/register', async (req, res, next) => {
    const data = req.body
    try {
      await userData.exist(data.user)
    } catch ( err ) {
      return res.status( err.code ).json( {message: err.message } )
    }     

    try {
      const createdUser = await userData.add(data)
      const authToken = createJSONToken(createdUser.user)
      return res
              .status(201)
              .json({ message: 'Usuario creado', user: createdUser, token: authToken })
    } catch (error) {
      return res.json(error)
    }
})

router
  .post('/login', async (req, res) => {
    const user = req.body.user
    const password = req.body.password
    let userResult
    try {
      userResult = await userData.get( user )
    } catch (error) {
      return res.status( error.code ).json({ message: error.message })
    }
    const pwIsValid = await isValidPassword(password, userResult.password)
    console.log(pwIsValid)
    if (!pwIsValid) {
      return res.status(422).json({
        message: 'Las credenciales no son correctas.',
      })
    }
    const token = createJSONToken( user )
    res.json({ token })
})

export default router
