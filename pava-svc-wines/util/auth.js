//const { sign, verify } = require('jsonwebtoken')
import jsonwebtoken from 'jsonwebtoken'
import bcrypt from 'bcryptjs'

const KEY = 'supersecreta'

export const createJSONToken = ( user ) => {
  return jsonwebtoken.sign( { user }, KEY, { expiresIn: '1h' })
}

export const validateJSONToken = (token) => {
  return verify(token, KEY)
}

export const isValidPassword = async (password, storedPassword) => {
  return bcrypt.compare(password, storedPassword)
}
