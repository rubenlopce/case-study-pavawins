import pg from 'pg'
import { credentials } from './credentials.js'

export const getUser = async ( user ) => {
    let result
    const query = `SELECT * FROM "users" WHERE "user" = '${user}'`
    const pool = new pg.Pool(credentials)
    try {
      result = await pool.query(query)
      if(result.rowCount === 0){
        result = null
      }else{
        result = result.rows[0]
      }
    }catch ( e ) {
      console.log(e)
      await pool.end()
      return e
    }
    await pool.end()
    return result
}

export const createUser = async ( user, password ) => {
  let result
  const query = `INSERT INTO "users" 
                  ("user", "password")
                  VALUES('${user}', '${password}')`

  const pool = new pg.Pool(credentials)
  try {
    result = await pool.query(query)
  }catch ( e ) {
    await pool.end()
    return e
  }
  await pool.end()
  return result
}

export const getMeasuraments = async () => {
  let measuraments = []
  const query = `select 
	                "year", 
	                "v"."variety",
	                "t"."type",
                  "color",
	                "temperature",
	                "calibration",
	                "ph",
	                "observations"
	                from 
	                	"wines" "w"
	                left join "variety" "v"
	                	on "w"."variety" = "v"."id"
	                left join "type" "t"
	                	on "w"."type"  = "t"."id"`
  const pool = new pg.Pool(credentials)
  try {
    const result = await pool.query(query)
    result.rows.forEach(element => {
        measuraments.push(element)
    })
  }catch ( e ) {
    await pool.end()
    return e
  }

  await pool.end()

  return measuraments
}

export const createMeasurament = async ( data ) => {
  let result
  let query
  if(data.observations){
    query = `insert into "wines" 
              ("year", "variety", "type", "color", "temperature", "calibration", "ph", "observations")
	            VALUES('${data.year}', ${data.variety}, ${data.type}, '${data.color}', ${data.temperature}, ${data.calibration}, ${data.ph}, '${data.observations}')`
  }else{
    query = `insert into "wines" 
              ("year", "variety", "type", "color", "temperature", "calibration", "ph", "observations")
              VALUES(${data.year}, ${data.variety}, ${data.type}, '${data.color}', ${data.temperature}, ${data.calibration}, ${data.ph}, null)`
  }

  const pool = new pg.Pool(credentials)

  try {
    result = await pool.query(query)
  }catch ( e ) {
    await pool.end()
    return e
  }

  await pool.end()

  return result
}

export const getTypes = async () => {
  let types = []
  const query = `SELECT * FROM "type"`

  const pool = new pg.Pool(credentials)

  try {
    const result = await pool.query(query)
    result.rows.map( (value, index) => {
      types[index] = { 'value': value.id, 'name': value.type }
    })
  }catch ( e ) {
    await pool.end()
    return e
  }

  await pool.end()

  return types
}

export const getVarieties = async () => {
  let varieties =  []
  const query = `SELECT * FROM "variety"`

  const pool = new pg.Pool(credentials)

  try {
    const result = await pool.query(query)
    result.rows.map( (value, index) => {
      varieties[index] = { 'value': value.id, 'name': value.variety }
    })
  }catch ( e ) {
    await pool.end()
    return e
  }

  await pool.end()

  return varieties
}