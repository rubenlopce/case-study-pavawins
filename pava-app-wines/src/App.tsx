import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import RootLayout from '@pages/Root'
import ErrorPage from '@pages/Error'
import HomePage, { action as AddMeasureAction } from '@pages/Home'
import LoginPage, { action as AuthAction } from '@pages/Auth'
import { action as LogoutAction } from '@pages/Logout'
import { checkAuthLoader, tokenLoader} from '@util/auth'

import './App.scss'

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    loader: tokenLoader,
    id: 'root',
    children: [
      {
        index: true,
        element: <HomePage />,
        loader: checkAuthLoader,
        action: AddMeasureAction
      },
      { 
        path: '/login', 
        element: <LoginPage />,
        action: AuthAction,
      },
      { 
        path: '/logout', 
        action: LogoutAction
      }
    ],
  }
])

export const App = () => {
  return <RouterProvider router ={router} />
}

export default App
