interface Wines {
  id?: number
  year:number,
  variety: string,
  type: string,
  color: string,
  temperature: number,
  calibration: number,
  ph: number,
  observations: string
}

export default Wines