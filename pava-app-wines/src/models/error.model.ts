interface ErrorModel {
  message: string,
  status?: number
}

export default ErrorModel