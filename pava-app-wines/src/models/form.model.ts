export interface SelectForm {
  value: number,
  name: string
}

export default SelectForm