export const getCurrentHost = () => {
  const host = import.meta.env.MODE === 'development'
  ? 'http://localhost:8080/'
  : ''

  return host
}

export default getCurrentHost