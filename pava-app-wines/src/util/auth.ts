import { redirect } from "react-router-dom"

export const getTokenDuration = () => {
  const storedExpirationDate = parseInt( localStorage.getItem( 'expiration' ) || '0' )
  const now = new Date().getTime()
  const duration = storedExpirationDate - now
  return duration
}

export const getAuthToken = () =>{
  const token = localStorage.getItem('token')

  const tokenDuration: number = getTokenDuration()

  if(tokenDuration < 0) {
    return 'EXPIRED'
  }

  return token
}

export const tokenLoader = () => {
  return getAuthToken()
}

export const checkAuthLoader = () => {

  const token = getAuthToken()

  if ( !token ) {

    return redirect('/login')

  } 

  return null

}