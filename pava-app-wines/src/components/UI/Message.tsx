import './Message.scss'

const Message = (props:any) => {
  const classes = 'message ' + props.className
  return <div className={classes}>{props.children}</div>
}

export default Message