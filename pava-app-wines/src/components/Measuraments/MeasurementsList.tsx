import { useCallback, useEffect, useMemo, useState } from 'react'
import Wines from '@models/wines.model'
import { MaterialReactTable } from 'material-react-table'
import { useTranslation } from 'react-i18next'
import getCurrentHost from '@util/host'

const MeasuramentsList = ( props: any ) => {
    const shouldReload = props.shouldReload === 'update'

    const { t } = useTranslation()
    const currentHost = getCurrentHost()
    const columns = useMemo(
      () => [
        {
          accessorKey: 'year', //access nested data with dot notation
          header: t( 'measuraments.year' ),
          size: 50,
        },
        {
          accessorKey: 'variety',
          header: t( 'measuraments.variety' ),
          size: 50,
        },
        {
          accessorKey: 'type', //normal accessorKey
          header: t( 'measuraments.type' ),
          size: 50,
        },
        {
          accessorKey: 'color',
          header: t( 'measuraments.color' ),
          size: 50,
        },
        {
          accessorKey: 'temperature',
          header: t( 'measuraments.temperature' ),
          size: 50,
        },
        {
          accessorKey: 'calibration',
          header: t( 'measuraments.calibration' ),
          size: 50,
        },
        {
          accessorKey: 'ph',
          header: t( 'measuraments.ph' ),
          size: 50,
        },
        {
          accessorKey: 'observations',
          header: t( 'measuraments.observations' ),
          size: 150,
        },
      ],
      [],
    )

    const [ measuraments, setMeasuraments ] = useState<Wines[]>([])
    const [ isLoading, setIsLoading ] = useState(true)

    const fetchMeasuramentsHandler = useCallback( async () => {
      let data: any = []
      if (!data.length) {
        setIsLoading(true)
      }
      const response = await fetch(currentHost + 'measuraments')
      data = await response.json()
      setMeasuraments( data.measuraments )
      setIsLoading(false)
    }, [])
  
    useEffect( () => {
      fetchMeasuramentsHandler()
    }, [fetchMeasuramentsHandler, shouldReload])

  if(isLoading && shouldReload){
    return <h2 className='loading'>{ t( 'general.loading' ) }</h2>
  }

  if( measuraments.length === 0 ){
    return <h2 className='loading'>{ t( 'general.loading' ) }</h2>
  }

  return <ul className='measuraments-list'>
          <MaterialReactTable 
            columns={columns} 
            data={ measuraments}
            enableColumnActions={false}
            enableColumnFilters={false}
            enablePagination={false}
            enableBottomToolbar={false}
            enableTopToolbar={false} /> 
        </ul>

}

export default MeasuramentsList