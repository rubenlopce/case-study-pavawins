import Card from "@components/UI/Card"
import getCurrentHost from "@util/host"
import { useCallback, useEffect, useState } from "react"
import { useTranslation } from "react-i18next"
import { Form, useNavigation } from "react-router-dom"
import FormSelect from '@models/form.model'

import './MeasuramentForm.scss'

const MeasuramentForm = ( props: any ) => {
  const { t } = useTranslation()
  const navigation = useNavigation()
  const currentHost = getCurrentHost()

  const isSubmitting = navigation.state === 'submitting'

  const [ varieties, setVarieties ] = useState<FormSelect>([])
  const [ types, setTypes ] = useState<FormSelect>([])
  const [ isLoading, setIsLoading ] = useState(true)

  const fetchSelectHandler = useCallback( async () => {
    let data: any = []
    if (!data.length) {
      setIsLoading(true)
    }
    const response = await fetch(currentHost + 'form-select')
    data = await response.json()
    setVarieties (data.varieties)
    setTypes (data.types)
    setIsLoading(false)
  }, [])

  useEffect( () => {
    fetchSelectHandler()
  }, [fetchSelectHandler])

  if(isLoading){
    return <h2 className='loading'>Cargando...</h2>
  }

  if(!isSubmitting){
    props.onMeasureAdded('update!')
  }else{
    props.onMeasureAdded('loading...')
  }

  return <Card>
          <h1 className='title'>Añadir mediciones</h1>
          <Form method='post' className='measurament_form'>
            <p className='measurament_field_container col-1'>
              <label htmlFor='year'> { t( 'measuraments.year' )} </label>
              <input id='year' type='number' name='year' required/>
            </p>
            <p className='measurament_field_container col-2'>
              <label htmlFor='variety'> { t( 'measuraments.variety' )} </label>
              <select id='variety' name='variety'>
                { 
                  varieties.map( (variety:FormSelect) => <option key={variety.value} value={variety.value}>{variety.name}</option>)
                }
              </select>
            </p>
            <p className='measurament_field_container col-2'>
              <label htmlFor='type'> { t( 'measuraments.type' )} </label>
              <select id='type' name='type'>
                { 
                  types.map( (type:FormSelect) => <option key={type.value} value={type.value}>{type.name}</option>)
                }
              </select>
            </p>
            <p className='measurament_field_container col-2'>
              <label htmlFor='color'> { t( 'measuraments.color' )} </label>
              <input id='color' type='text' name='color'  required/>
            </p>
            <p className='measurament_field_container col-2'>
              <label htmlFor='temperature'> { t( 'measuraments.temperature' )} </label>
              <input id='temperature' type='number' name='temperature'  required/>
            </p>
            <p className='measurament_field_container col-2'>
              <label htmlFor='calibration'> { t( 'measuraments.calibration' )} </label>
              <input id='calibration' type='number' name='calibration'  required/>
            </p>
            <p className='measurament_field_container col-1'>
              <label htmlFor='ph'> { t( 'measuraments.ph' )} </label>
              <input id='ph' type='number' name='ph'  required/>
            </p>
            <p className='measurament_field_container col-9'>
              <label htmlFor='observations'> { t( 'measuraments.observations' )} </label>
              <input id='observations' type='text' name='observations' />
            </p>
            <p className='measurament_field_container col-3 center'>
              <button className='col-4 ghost thin' disabled={ isSubmitting } >
                { isSubmitting ? t( 'form.submitting' ) : t( 'form.submit' ) }
              </button>
            </p>
          </Form>
        </Card>
}

export default MeasuramentForm