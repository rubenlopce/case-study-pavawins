import { useState } from "react"
import MeasuramentForm from '@components/Measuraments/MeasuramentForm'

const NewMeasurament = ( props: any ) => {
  const addMeasureHandler = ( didUpdate ) => {
    props.onMeasureAdded(didUpdate)
  }

  return(
    <div className="new-measurament">
      <MeasuramentForm onMeasureAdded={addMeasureHandler} />
    </div>
  )
}

export default NewMeasurament