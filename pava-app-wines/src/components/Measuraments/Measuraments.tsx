import MeasuramentsList from "./MeasurementsList"
import Card from "@components/UI/Card"

const Measuraments = ( props: any ) => {
  const shouldReload = props.didUpdate

  return <Card className='list'>
          <MeasuramentsList shouldReload={shouldReload} />
        </Card>  
}

export default Measuraments