import { useTranslation } from 'react-i18next'
import { Form, Link, useNavigation, useSearchParams } from 'react-router-dom'
import Card from '../UI/Card'

import './AuthForm.scss'
import FormErrors from './FormErrors'

export const AuthForm = () => {
  const { t } = useTranslation()
  const navigation = useNavigation()
  const [ searchParams ] = useSearchParams()
  
  const isSubmitting = navigation.state === 'submitting'

  const isLogin = searchParams.get('mode') === 'login'

  return <Card className='auth'>
          <Form method='post' className='form'>
            <h1 className='title'>{ isLogin ? t( 'auth.login' ) : t( 'auth.register' ) }</h1>

            <FormErrors />

            <p className='form_field'>
              <label htmlFor='user'>{ t( 'auth.user' ) }</label>
              <input id='user' type='text' name='user' required />
            </p>

            <p className='form_field'>
              <label htmlFor='pass'>{ t( 'auth.pass' ) }</label>
              <input id='pass' type='password' name='pass' required />
            </p>
            
            <button id={ isLogin ? 'btn-register' : 'btn-login'} className='ghost' disabled={ isSubmitting } >
            { isSubmitting ? t( 'form.submitting' ) : t( 'form.submit' ) }
            </button>

          </Form>

          <Link to={`?mode=${ isLogin ? 'register' : 'login'}` } className='btn-change'>
            { !isLogin ? t( 'auth.iniciar' ) : t( 'auth.crear' ) }
          </Link>

        </Card>
}

export default AuthForm