import { useActionData } from 'react-router-dom'
import Message from '../UI/Message'

import ErrorModel from '@models/error.model'

const FormErrors = () => {
  const data = useActionData() as ErrorModel
  
  return <>
          { data && 
          <Message className='error'>
            <p>
              { data.message }
            </p>
          </Message> 
          }
        </>
}

export default FormErrors