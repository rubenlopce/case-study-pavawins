import Logo from '@assets/logo.svg'
import AuthForm from '@components/Auth/AuthForm'
import getCurrentHost from '@util/host'
import { t } from 'i18next'
import { useEffect } from 'react'
import { json, redirect, useSearchParams } from 'react-router-dom'

export const LoginPage = () => {

  const [searchParams, setSearchparams] = useSearchParams()
  const currentMode = searchParams.get('mode')

  useEffect(() => {
    if (!currentMode) {
      searchParams.set('mode', 'login')
      setSearchparams(searchParams)
    }
  }, [searchParams, setSearchparams, currentMode])

  return <>
          <img src={Logo} alt="Logo PavaWines" /> 
          <AuthForm />
        </>
}

export default LoginPage

export const action = async ( { request } ) => {

  const searchParams = new URL(request.url).searchParams
  const mode = searchParams.get( 'mode' ) || 'login'
  
  const data = await request.formData()
  const authData = {
    user: data.get( 'user' ),
    password: data.get( 'pass' )
  }
  
  const currentHost = getCurrentHost()
  const response = await fetch( currentHost + mode, {
    method: 'POST',
    headers: {
      'Content-type': 'application/json'
    },
    body: JSON.stringify(authData)
  })
  
  if( response.status === 422 || response.status === 401 || response.status === 404 ){
    return response
  }

  if( !response.ok ){
    throw json( {message: t( 'auth.errorKO' )}, { status: 500 } )
  }

  const resData = await response.json()
  const token = resData.token

  localStorage.setItem('token', token)

  let expiration = new Date().getTime()
  expiration = expiration + (3600 * 1000)
  localStorage.setItem('expiration', expiration.toString())

  return redirect('/')
}