import Measuraments from '@components/Measuraments/Measuraments'
import NewMeasurament from '@components/Measuraments/NewMeasurament'
import Wines from '@models/wines.model'
import { getAuthToken } from '@util/auth'
import getCurrentHost from '@util/host'
import { useState } from 'react'
import { json, redirect, useNavigation } from 'react-router-dom'


export const Home = () => {
  const [ didUpdate, setDidUpdate ] = useState<String>()

  const addMeasureHandler = ( didUpdate ) =>{
    if(didUpdate==='update!'){
      setDidUpdate('update')
    }else{
      setDidUpdate('updaten\'t')
    }
  }

  return <>
          <NewMeasurament onMeasureAdded={addMeasureHandler} />
          <Measuraments didUpdate={didUpdate} />
        </>
}

export default Home

export const action = async ( { request } ) => {

  await timeout(1000)
  const data = await request.formData()
  const measuramentData: Wines = {
    year: data.get('year'),
    type: data.get('type'),
    variety: data.get('variety'),
    color: data.get('color'),
    temperature: data.get('temperature'),
    calibration: data.get('calibration'),
    ph: data.get('ph'),
    observations: data.get('observations'),
  }

  const currentHost = getCurrentHost()

  const token = getAuthToken()

  const response = await fetch( currentHost + 'measuraments', {
    method: 'POST',
    headers: {
      'Content-type': 'application/json',
      'Authorization': 'Bearer ' + token 
    },
    body: JSON.stringify(measuramentData)
  })
  
  if( response.status === 422 || response.status === 401  ){
    return response
  }

  if( !response.ok ){
    throw json( {message: 'auth.errorKO' }, { status: 500 } )
  }

  return redirect('/')

}

function timeout(delay: number) {
  return new Promise( res => setTimeout(res, delay) )
}