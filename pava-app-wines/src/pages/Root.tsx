import Navbar from '@components/UI/Navbar'
import { getTokenDuration } from '@util/auth'
import { useEffect } from 'react'
import { Form, Outlet, useLoaderData, useSubmit } from 'react-router-dom'

export const RootLayout = () => {
  const token = useLoaderData()
  const submit = useSubmit()

  useEffect( () =>{
    if ( !token ) {
      return
    }

    if ( token === 'EXPIRED' ) {
      localStorage.removeItem('expiration')
      submit(null, {action: '/logout', method: 'post'})
      return
    }

    const tokenDuration = getTokenDuration()

    setTimeout(() => {
      submit(null, {action: '/logout', method: 'post'})
    }, tokenDuration )

  }, [token, submit])

  const logoutButton = 
        <Navbar>
          <div className='logo' role='image'></div>
          <Form action='/logout' method='post'>
            <button>
              Cerrar sesión
            </button>
          </Form>
        </Navbar>

  return <>
          { token && token !== 'EXPIRED' && logoutButton }
          <Outlet />
        </>
}

export default RootLayout