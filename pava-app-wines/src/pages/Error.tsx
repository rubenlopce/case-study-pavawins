import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import Card from '../components/UI/Card'
import Logo from '@assets/logo.svg'

export const ErrorPage = () => {

  const { t } = useTranslation()

  return <Card>
          <img src={Logo} alt="Logo PavaWines" /> 
          <h1>{t('general.error404')}</h1>
          <Link to='/'><button className='standalone'>{t('general.back')}</button></Link>
        </Card>
}

export default ErrorPage